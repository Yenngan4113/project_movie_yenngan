import React from "react";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import { localService } from "../Services/localService";
import { Dropdown, Menu, Space } from "antd";

export default function Layout({ Component }) {
  let user = useSelector((state) => state.userSlice.user);

  const menu = (
    <Menu
      items={[
        {
          label: "Đăng xuất",
          key: "1",
          onClick: () => {
            localService.removeUserInfor();
            window.location.href = "/";
          },
        },
        {
          label: "Quay lại trang chủ",
          key: "2",
          onClick: () => {
            window.location.href = "https://movieproject-8af40.web.app/";
          },
        },
      ]}
    />
  );
  return (
    <div className="container mx-auto">
      <div className="container flex justify-between mx-auto h-20 items-center border-2">
        <div className="w-1/6 border-r-2 text-xl font-medium text-orange-700 h-full flex items-center justify-center">
          DASHBOARD
        </div>
        {!user ? (
          <div></div>
        ) : (
          <div className="pr-12">
            Xin chào{" "}
            <Dropdown overlay={menu} placement="bottomRight">
              <a onClick={(e) => e.preventDefault()}>
                <span className="text-lg text-orange-700">{user.hoTen}</span>
              </a>
            </Dropdown>
          </div>
        )}
      </div>
      <div className="flex border-2 border-t-0">
        <div className="w-1/6 flex flex-col border-r-2">
          <NavLink to="/">
            <button className="text-lg font-medium mt-8 mb-4 py-2 w-full focus:bg-gray-200 hover:text-black text-black">
              Quản lý người dùng
            </button>
          </NavLink>
          <NavLink to="/filmmanage">
            <button className="text-lg font-medium py-2 w-full focus:bg-gray-200 hover:text-black text-black">
              Quản lý phim
            </button>
          </NavLink>
        </div>
        <div className="w-5/6">
          <Component />
        </div>
      </div>
    </div>
  );
}
