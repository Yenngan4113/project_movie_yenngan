import logo from "./logo.svg";
import "./App.css";
import "./FilmManagementPage/Film.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import UserManagePage from "./pages/UserManagePage/UserManagePage";
import LoginPage from "./pages/LoginPage/LoginPage";
import Layout from "./HOC/Layout";
// import FirmManagePage from "./pages/FirmManagePage/FirmManagePage";
import Dashboard from "./pages/Dashboard/Dashboard";
import FilmManagementPage from "./FilmManagementPage/FilmManagementPage";
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDWvzp5-lt7IEPhJSur1m2L_4Eh4wjUdLk",
  authDomain: "movieproject-8af40.firebaseapp.com",
  projectId: "movieproject-8af40",
  storageBucket: "movieproject-8af40.appspot.com",
  messagingSenderId: "1095404667536",
  appId: "1:1095404667536:web:23b68e3c2487d8c2dfce15",
  measurementId: "G-MPY42N54PH",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={Dashboard} />} />
          <Route
            path="/usermanage"
            element={<Layout Component={UserManagePage} />}
          />
          <Route
            path="/filmmanage"
            element={<Layout Component={FilmManagementPage} />}
          />
          <Route path="/login" element={<LoginPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
